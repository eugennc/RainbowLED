![neopixel](pics/neopixel.jpg)
Abstract
==

Acest fisier contine instructiunile principale pentru a realiza un curcubeu de 
lumini LED, fara cunostinte avansate de electronica.

Abilitati necesare
==

Realizarea acestui proiect va necesita:
 * cumpararea de componente electronice
 * taierea unor mici bucati de plastic moale
 * indoirea unor fire de cupru subtiri, relativ moi
 * instalarea unui mic mediu de programare pe calculator
 * urmarea unor tutoriale simple in limba engleza, despre instalarea acelui 
   mediu de programare
 * copierea unui program gata-facut, si instalarea sa prin mediul de programare
 * optional, coaserea sau impletirea unui sistem de prindere; 
   lucrul cu materiale textile, ac si ata.

Nu este nevoie de efectuarea unor lipituri electronice.

Materiale necesare
==

Minimal, este nevoie de urmatoarele componente:
 * Calculator. Windows/Linux/Mac.
 * Controller programabil - am folosit un [gemma](https://www.adafruit.com/gemma).
 * Inel/banda de LED-uri - am folosit un [NEOPIXEL](https://www.adafruit.com/product/1463).
 * Fire electronice - am desfacut un cablu de retea.
 * Foarfece sau cutter: pentru a indeparta izolatia de plastic de pe fire.
 * Tesatura: am folosit o panza de in destul de rara, se poate folosi cam 
   orice panza mai tare.
 * Ac si ata (preferabil ceva mai groasa si rezistenta), pentru a coase.
 * Sistem de prindere: am folosit Velcro (arici, scai).
 * Acumulator/powerbank pentru mobile - unul mic e perfect OK.
 * Fir USB Micro-B pentru incarcare - de obicei, vine cu powerbank-ul. 
 * Fir USB Micro-B de date - vin cu majoritatea telefoanelor mobile. Cablul USB
 de date va permite sa si alimentati cu energie, deci nu e nevoie de cablu 
 suplimentar de incarcare.

Obtinere materiale
==
Pret & de unde:
 * Controller - [69 RON](https://www.robofun.ro/adafruit-gemma-miniature-wearable-electronic-platform)
 * Inel de LEDuri - [67 RON](https://www.robofun.ro/neopixel-ring-16-x-ws2812-5050-rgb-led)
 * Cablu de retea - sub 10 RON. Se pot cumpara 50cm de cablu nemufat de la orice 
 magazin de electronice sau calculatoare generic. Mai scump este cel mufat. 
 O buna alternativa este un cablu vechi sau stricat, intrucat il stricam oricum.
 * Foarfece: probabil aveti deja.
 * Tesatura: avem nevoie de foarte putina. Am luat 25cm liniari de panza la 6 RON,
   si a fost mult, mult prea multa.
 * Ac - 1 RON
 * Ata - o papiota de ata mai rezistenta - 3 RON.
 * Velcro/arici/scai - avem nevoie de vreo 10 cm, costa sub 3 RON.
 * Acumulator/powerbank - daca nu aveti unul, chiar si mai vechi sau mic, 
   puteti sa gasiti incepand cu 20 RON, de la magazine de electronice.
 
## Explicatii suplimentare

Pentru controller si inel de LED-uri, am dat link-urile de unde am cumparat eu. 
Nu spun ca e cel mai ieftin, doar ca mi-a fost mie la indemana. Componentele
le-am ales fiindca sunt mici, consuma foarte putin, si sunt gandite sa fie
usor de cusut/integrat in haine.
In rest, am incercat sa fac proiectul cat mai ieftin si usor de construit, 
folosind 'ce-i prin casa' sau recicland.

Ce este un LED? Un LED este o componenta electronica ce emite lumina 
(LED - Light Emitting Diode). In general, sunt mici, emit lumina puternica, si 
consuma relativ putin.

Controller-ul este un mic calculator programabil, care va face ce ii spunem - 
sa aprinda luminile cu anumite culori. 

Inelul de LED-uri are mai multe LED-uri ce pot emite cam orice nuanta de culoare,
si poate sa fie controlat foarte precis si usor de catre un controller.

In mare, avem 4 sarcini:
 * Sa legam electric, prin fire, controller-ul de LED-uri, astfel incat 
 controller-ul sa poata controla LED-urile.
 * Sa programam controller-ul folosind un calculator.
 * Sa facem un sistem de fixare si sustinere pentru LED-uri.
 * Sa conectam un powerbank la controller, care sa ii permita sa functioneze
 departe de calculatorul care l-a programat.

Asamblare
==

In primul rand, eu am facut acest proiect facand cateva lipituri electrice, si 
adaugand cateva elemente in plus (mufe de conectare, fire in plus). Fotografiile
le-am facut dupa ce proiectul a fost asamblat si functional. In principiu, 
puteti ignora componentele in plus.

In al doilea rand, va recomand ca cititi tot tutorial-ul, si abia apoi sa incepeti
sa lucrati. Pasii din tutorial va vor influenta lungimile firelor, unde 
puteti/vreti sa plasati sau purtati componentele, etc.

Incepem proiectul prin a ne pune materialele la dispozitie. In special, va trebui
sa obtinem firele electrice. 

![Cablu](pics/netWire.jpg)

Incepem sa taiem cablul de retea pe o lungime 
suficient de lunga pentru firele noastre. Recomand cel putin 30 cm, oricand le 
putem scurta. Mai lung e mai bine. Puteti sa taiati invelisul exterior cu un 
foarfece sau cutter. Daca gasiti un invelis interior din staniol, indepartati-l.
Ne intereseaza firele din mijlocul cablului.

![Cablu](pics/netWireCut.jpg)

Din cele 8 cabluri, avem nevoie de 3. Cablurile sunt grupate 2 cate 2. Dupa ce le
desfaceti individual, puteti folosi un foarfece mai vechi sa le taiati 
(ii va afecta taisul), sau puteti sa indoiti inainte si inapoi cablul intr-un 
punct. In cele din urma (vreo 2 minute), metalul se va rupe in acel punct. Cu
metalul rupt, puteti sa trageti de plastic sau sa il taiati.

![Cablu](pics/wireInsulated.jpg)
(firul e cam scurt in poza, e bun mai lung de 30 cm)

Acum, plasticul de la captele cablurilor va trebui indepartat. Puteti sa folositi
un cutter sau o singura lama a unui foarfece, sa inconjurati cablul intr-un punct.
Aveti grija sa nu taiati si metalul, sau sa il indoiti de prea multe ori - se va
rupe. Avem nevoie cam de 5 cm intr-un capat si 10 cm in celalalt (mai lung e mai 
bine). Daca e prea dificil de scos de pe fir, puteti sa il taiati si 
scoateti pe bucati. Puteti sa folositi o bricheta sau aragaz sa topiti PVC-ul de
pe fir, insa fumul scos e toxic - faceti asta doar afara sau cu geamul deschis.
Mai bine nu riscati, IMO.

![Cablu](pics/wireUninsulated.jpg)

Odata ce aveti cele 3 cabluri dezizolate (cu capetele metalice expuse), puteti sa
aduceti controller-ul si inelul de LED-uri. 

!!! Inainte de a le scoate din punga, este important sa atingeti o bucata de metal
cu impamantare, de exemplu o bucata nevopsita si neruginita a unui calorifer 
instalat. Sarcinile electrice statice pot, altfel, sa arda controller-ul sau 
inelul de LED-uri. Este cu atat mai important daca aveti parul lung, proaspat 
spalat, sau haine pufoase, caci acestea genereaza si pastreaza electricitate
statica mai usor. Daca faceti pauze de lucru, atingeti metal impamantat de fiecare
data cand reincepeti sa lucrati.

![Gemma](pics/gemmaBag.jpg)
![Pixel](pics/pixelBound.jpg)


Daca va uitati pe fata controller-ului, veti vedea, pe margini, 6 zone galben/maro,
cu text sub ele: 3Vo, Vout, GND, D0, D1, D2. Acestea sunt terminale electrice, si
trei dintre ele vor trebui conectate de la controller la LED-uri.

![Gemma](pics/gemmaBig.jpg)
![Pixel](pics/neopixelRing.jpg)

Intai procedura, si apoi ce trebuie legat de ce:

Intrucat ne-am propus sa nu facem lipituri electrice, vom indoi firele astfel incat
ele sa faca contact bine cu terminalele electrice. Aveti grija, totusi, sa nu
stricati controller-ul - aplicati forta firelor, nu controller-ului. Intrucat gaurile de pe
controller sunt marisoare, puteti sa folositi si mai mult fir dezizolat, si sa 
il 'coaseti' prin gaurile din contacte. Puteti sa treceti de 2 sau 3 ori cu firul,
de fiecare data indoindu-l in jurul sau, si apoi aplatizandu-l (cu grija, sa nu
stricati controller-ul). Cand treceti prima data cu firul prin gaura, aduceti 
partea izolata a cablului relativ aproape de controller, pe la vreo 0-3mm.
Aici ne ajuta lungimea mare de cablu neizolat, care face indoirea mai usoara. 
Daca cablul este bine fixat si ati mai ramas cu cablu dezizolat in plus, 
rupeti-l. Nu vrem ca aceste cabluri sa se atinga intre ele.

![Cablu](pics/wireWeave.jpg)

Pe inel-ul de LED-uri, se va repeta procesul. Faptul ca gaurile din contacte sunt
mai mici ne ajuta: este nevoie sa trecem firele de maximum 2 ori prin ele.
Daca nu sunteti siguri ca puteti sa legati trainic, puteti sa folosit staniol 
(de la ambalaje de ciocolata, de ex): puteti sa bagati mici fasii (nu ghemotoace)
in gauri inainte sa treceti firele pe acolo, si, dupa ce ati terminat de legat,
sa "tundeti" bucatile de staniol care pot atinge alte fire.


Acum ca stim cum sa legam, sa vedem ce sa legam de ce:
Din nou, uitandu-ne pe fata controller-ului si pe spatele inelului de LED-uri:
 * Controller "GND"  - Inel "Power Signal Ground"
 * Controller "Vout" - Inel "Power 5V DC"
 * Controller "D0"   - Inel "Data Input"

![Gemma](pics/gemmaBig.jpg)
![Pixel](pics/neopixelRing.jpg)

Daca considerati ca ati legat bine cele 6 contacte, putem sa trecem la programare.

Programare
==
Va trebui sa instalam pe calculator mediul de programare ce ne permite sa 
programam controller-ul. Pentru Windows, este nevoie de instalarea [driver-ului](https://learn.adafruit.com/introducing-gemma/windows-setup).
Linux si Mac nu au nevoie de driver-e suplimentare.

Acum puteti sa faceti download la [mediul de programare](https://learn.adafruit.com/adafruit-arduino-ide-setup/arduino-1-dot-0-x-ide), 
in functie de ce sistem de operare folositi.

Dupa ce ati extras si rulat mediul de programare "Arduino" o data, puteti sa il 
inchideti temporar. El ar trebui sa fi creat un director special. Pe Windows, este 
"My Documents\Arduino". Pe Linux si Mac, este in Home, "~/sketchbook".

Acum - faceti download la biblioteca FastLED (ea ne va permite sa lucram usor cu
inelul de LED-uri), versiunea [3.1.0](https://github.com/FastLED/FastLED/archive/v3.1.0.zip).

Va trebui sa o extrageti si sa redenumiti directorul extras, 
de la "FastLED-3.1.0" la "FastLed". Apoi, copiati sau mutati acest director 
(nu uitati sa creati subdirectorul libraries, daca nu exista):
* Windows My Documents\Arduino\libraries\FastLed
* Linux/MAC ~/sketchbook/libraries/FastLed

Acum: conectati controller-ul Gemma cu cablul USB de date. 
Verificati ca functioneaza: un mic LED verde de pe controller ar trebui sa fie
aprins. Nu e nici o problema daca alt mic LED rosu se aprinde si el.

Deschideti mediul de programare "Arduino" (cel pe care l-ati extras inainte) din nou. 
Verificati ca biblioteca FastLED a fost instalata bine: ar trebui sa aveti meniul

File>Examples>FastLED

![FastLED](pics/ideFastLed.png)

Daca totul e OK, configurati mediul de programare sa foloseasca Gemma, si sa 
foloseasca programatorul de controller potrivit:
![ideG](pics/ideGemma.png)

Tools>Board>Adafruit Gemma 8MHz


![ideP](pics/ideProgrammer.png)

Tools>Programmer>USBtinyISP

* Creati un nou proiect (File>New), gol.
* Stergeti tot codul din proiect, daca acesta nu este gol.
* Deschideti [rainbowLED.ino](rainbowLED.ino) si copiati continutul in noul proiect.
* Salvati proiectul (File>Save As). 

Verificati ca totul e OK folosind "Sketch>Verify / Compile" (sau butonul verde 
cel mai din stanga, de sub File, in stanga-sus a ferestrei)

![verify](pics/arduinoIdeButtonsVerify.png)

Daca nu au aparut erori, sunteti gata sa programati controller-ul. 

Controller-ul nu poate fi programat oricand; are 3 perioade de activitate:
* pornire
* disponibilitate de programare
* executie program

Trebuie sa il programam in perioada destul de scurta (vreo 10 secunde) de dupa pornire.

![labels](pics/flora_guide.jpg)

Pe schema de mai sus, puteti vedea indicate LED-ul rosu, LED-ul verde si butonul
de reset.

LED-ul verde ramane constant aprins, semnificand ca Gemma functioneaza/primeste
putere.
LED-ul rosu ne indica starea in care se afla Gemma; daca clipeste repede (de cateva
ori pe secunda), atunci Gemma este in modul de pornire. Tipic, aceast mod dureaza
2-3 secunde. Apoi, LED-ul rosu incepe sa pulsese mai incet - acum este perioada
in care il putem programa. Dupa aceasta perioada, Gemma incepe sa execute ce i-a
fost programat.

Pentru a intrerupe executia, si a intra in modul de programare, trebuie sa resetam
Gemma. Butonul de reset este mic, negru, si se afla intre cele doua LED-uri (cel
rosu si cel verde), dupa cum se vede in poza de mai sus.

Ce va trebui sa faceti: va trebui sa resetati Gemma apasand butonul, sa asteptati
2-3 secunde pana Gemma intra in modul de programare, apoi, din mediul de programare,
sa incarcati programul de pe calculator pe Gemma. Upload din mediul de programare
se face fie din File>Upload, sau din al doilea buton verde (in stanga-sus, sub Edit,
al doilea buton de la stanga, ce arata o sageata orizontala, spre dreapta).

![verify](pics/arduinoIdeButtonsUpload.png)

Modul meu preferat este sa apas Control-U, care este scurtatura de tastatura pentru
acest buton.

Pe scurt, pasii care mi se par mie cei mai simpli sunt:
* Asigurati-va ca Gemma este conectat la calculator.
* Asigurati-va ca programul este verificat.
* Intoarceti Gemma si puneti un deget peste butonul de reset, astfel incat sa
puteti vedea LED-ul rosu. Nu apasati inca.
* Puneti degetele de la cealalta mana pe tastatura, gata sa apasati Control-U. 
Nu apasati inca.
* Apasati butonul de reset de pe Gemma. Asteptati cele 2-3 secunde de clipire
rapida a LED-ului rosu. Cand LED-ul incepe sa clipeasca mai incet, apasati
Control-U. Daca mediul de programare afiseaza o eroare, mai apasati o data 
Control-U.
* Va puteti da seama ca programare s-a facut cu succes daca inelul de LED-uri se
aprinde.

Pasii de mai sus ar trebui sa mearga; insa, pot aparea multe probleme - din fericire,
multi oameni folosesc astfel de controller-e, si aveti sanse bune sa gasiti solutii
cautand pe Internet.

Eu m-am confruntat cu o singura problema: lucrand pe Linux, mediul de programare
nu putea gasi controller-ul. Am vazut ca era o problema de permisiuni, si am
urmat instructiunile de pe site-ul [producatorului](https://learn.adafruit.com/usbtinyisp/avrdude#for-linux).
Alternativ, mediul de programare poate fi rulat cu permisiuni de administrator
(sudo ./arduino).

Portabilizare
==

Daca doriti sa faceti sistemul portabil, va trebui sa il deconectati de calculator.
Din fericire, Gemma tine minte ce a fost programat, chiar daca il opriti/porniti
sau resetati din nou, si are nevoie doar de curent electric. Puteti sa folositi
un cablu scurt USB de incarcare, si sa il alimentati de la un powerbank de 
incarcat telefoane mobile. Chiar si pe cele mai mici powerbank-uri, ar trebui sa 
functioneze ore sau zeci de ore. 

Putem calcula, aproximativ, cat va tine proiectul pe un powerbank. Un powerbank 
are un numar asociat, ce reprezinta cat de multa energie poate stoca. In mod 
obisnuit, este notat in "mili-Amperi ora", sau "mAh", o conventie foarte utila
pentru calculul nostru. Ce spune? Daca avem un aparat care consuma acel numar
de mili-Amperi, el va putea fi alimentat o ora, inainte ca powerbank-ul sa se
descarce.

Sa presupunem ca avem un powerbank de 1000 mAh. Sa mai presupunem ca e vechi de 3
ani, asa ca hai sa ii taiem capacitatea la jumatate: 500 mAh. Din ce am masurat
sistemul cu programul meu ruland, consuma in medie 75 mAh. Gemma consuma vreo 
15mAh. Sa zicem 100 mAh, total. Ar trebui sa tina vreo 5 ore.

Deci, sistemul consuma destul de putin, intrucat ca am pus un delay() in program pe
Gemma (si controller-ul sta degeaba majoritatea timpului, neconsumand prea mult),
si fiindca nu avem luminozitate maxima decat ~1/10 din timp si nu folosim culoarea
alba (ce ar aprinde mai multe LED-uri).

Cum sa il portabilizam? Ar fi bine sa rigidizam putin instalatia, eventual lipind
contactele cu putina banda adeziva, si lipind firele intre ele cu mai multa
banda adeziva. Intrucat firele sunt doar infasurate pe contacte, nu folositi
superglue sau alti adezivi lichizi. Daca aveti timp, puteti infasura firele cu ata mai
groasa, si le puteti intretese, eventual legandu-le si de Gemma, trecand ata prin
gaurile nefolosite (D1, D2, 3Vo). Gaurile libere de pe inelul de LED-uri sunt mai
mici, si puteti fie sa folositi un ac mai mic, fie sa bagati ata de mana prin ele.

Am folosit un mic saculet de panza de in groasa, cusut cu ata mai rezistenta.
Am cusut de exteriorul lui o mica bucata de velcro. Apoi, am pus Gemma dupa ce 
l-am conectat si programat.
Dupa ce m-am asigurat ca am scos prin gura sacului cablul USB si firele de 
conectare, am folosit un fir de ata trecut prin tesatura sa strang si sa leg 
(cu funda/nod) gura saculetului. 

![pixelBound](pics/pixelBound.jpg)

Inelul de LED-uri l-am cusut si fixat cu ata de alta bucata de velcro. Spatiile 
dintre LED-uri mi-au permis sa il fixez destul de bine.

Intrucat aveam deja un buzunar cu inchidere cu velcro, nu a mai fost nevoie sa cos
bucatile complementare de velcro pe haine, si am tinut powerbank-ul in buzunar.

Testare
==

Am mers cu sistemul vreo 11 ore, cu el fixat deasupra genunchiului. M-am aplecat,
am mers repede, dar nu am fugit, tot timpul. A tinut foarte bine. Am folosit 
un powerbank destul de mare, de 10000 mAh, si acesta nu a putut detecta consumul 
efectuat - avand 4 nivele de raportare, a raportat ca era mai aproape de 100% 
decat de 75% dupa cele 11 ore.

Lumina LED-urilor a fost perfect vizibila intr-o zi de vara cu nori acoperind 
partial soarele, si a devenit orbitoare pe seara.

Alte idei
==
In loc de a folosi fire de cupru, exista ate electroconductive: 
[aici](https://learn.adafruit.com/conductive-thread), 
[aici](https://www.robofun.ro/etextil/fir-textil-conductiv-3-straturi-18-metri) 
sau [aici](https://www.robofun.ro/etextil/bobina-fir-textil-conductor-9.14m-stainless-steel).

Acestea pot, simultan, fixa componentele de o tesatura si pot conduce electricitate.

![flora](pics/flora_DSC_0113.jpg)

Mai multe idei in acest [tutorial](https://learn.adafruit.com/conductive-thread/overview) 
(eng).

Puteti sa faceti un sistem de prindere cu magneti. Ati putea folosi 2 magneti 
sau un magnet si o moneda de 10 bani. Un magneti mic si puternic costa cam
[9 RON](https://www.robofun.ro/magnet-neodymium). Cat de bine prind acesti 
magneti depinde de cat de mare e distanta dintre ei - ar trebui sa fie OK printr-un
tricou, dar nu printr-un palton. Trebuie sa aveti grija sa nu ii lasati sa se
ciocneasca (sunt puternici, si au tendinta de a se ciobi daca se ciocnesc).
Un sistem de prindere cu magneti ar trebui sa tina un NEOPIXELRING pe un tricou,
insa nu NEOPIXEL + Gemma + powerbank. Similar, s-ar putea desprinde daca fugiti.

Daca va pricepeti sa lipiti cu aliaj de lipire si pistol/ciocan de lipire, va 
sfatuiesc sa folositi fire lițate (care nu au doar un fir gros de cupru, ci mai 
multe fire de cupru, mai subtiri, si mai usor de indoit si lipit cu metal topit).
Va recomand aliaj fara plumb (99%+ staniu 1%-0.3% cupru e ok), si fire lipite 
intre ele. Ce am folosit eu nu se desprindeau prea bine de mana, dar cu un 
foarfece le puteam separa perfect.

![rainbowWires](pics/flatWires.jpg)
