//biblioteca FastLED
#include "FastLED.h"

//tipul de LED-uri pe care il avem
#define STRIP_TYPE NEOPIXEL
//cate LED-uri are inelul de LED-uri?
#define NUM_LEDS 16

//de pe controller, am folosit D0, D1, sau D2? Am folosit D0, deci aici avem 0
#define DATA_PIN 0

// cream o structura de date pe care o intelege FastLED
CRGB leds[NUM_LEDS];

//folosim o schema de culoare HSV (hue, saturation, brightness - nuanta, saturatie, stralucire)
int hue = 0;

//stralucirea minima si maxima intre care sa varieze LED-urile
int briMin = 63, briMax = 255;

float bri = briMin, briDir = 0.1; //briDir - daca acum stralucirea creste sau descreste

void setup() {
  //inregistram LED-urile noastre cu biblioteca FastLED 
  FastLED.addLeds<STRIP_TYPE,DATA_PIN>(leds, NUM_LEDS);
}

//in aceasta functie se creeaza curcubeul
void patternRainbow() {
  //prin variatia nuantei de la 0 la 255, trecem prin toate culorile curcubeului (asa este conceputa schema HSV, foarte convenabil)
  //ca sa avem tot curcubeul reprezentat de un numar de LED-uri, fiecare trebuie sa se afle la o 'distanta in culoare' de cel dinaintea lui
  //De asta impartim aici - sa stim care e distanta de culoare dintre 2 LED-uri succesive
  int hueInc = 255 / NUM_LEDS;
  
  //iteram prin toate LED-urile
  for(int ii = 0; ii < NUM_LEDS; ++ii) {
    //fiecarui LED ii dam o culoare astfel incat, in fiecare moment, sa avem un curcubeu
    //mai adaugam si o cantitate constanta la toate nuantele; aceasta se schimba doar de la un moment de timp la altul;
    //ea ne permite sa avem un curcubeu care se roteste, in timp.
    //folosind si parametrul de stralucire, facem curcubeul sa pulseze.
    leds[ii] = CHSV((hueInc * ii + hue) % 256, 255, int(bri));
  }
  //facem   hue MOD 256   sa nu avem valoarea hue mult prea mica, daca programul nostru ruleaza foarte mult timp
  hue = (hue - 1) % 256;

  //daca luminozitatea trebuie sa creasca, pai sa creasca
  if(briDir > 0) {
    bri *= 1.01;
  } else {
    //la fel descresterea
    bri /= 1.01;
    // folosesc o inmultire/impartire ca sa fac perioada de lumina maxima sa dureze destul de putin. 
    //Pe vreme insorita, NeopixelRing e perfect vizibil; inauntru, poate fi aproape orbitor.
  }

  //daca ma aflu la capatul minim al luminozitatii, schimb directia - incep sa cresc luminozitatea
  if(bri < briMin) {
    bri = briMin;
    briDir = -briDir;
  }
  //similar la capatul maxim
  if(briMax < bri) {
    bri = briMax;
    briDir = -briDir;
  }
}

//aceasta e bucla pe care o executa controller-ul mereu. 
//Daca stiti C/C++, e ca main(), doar ca se apeleaza, succesiv, mereu.
//Puteti sa ganditi ca pe controller este deja o functie main care face:
//main() {
//  while(true) { loop(); }
//}
void loop() {
  //apelam functia care deseneaza curcubeul
  patternRainbow();
  
  //odata ce am stabilit culorile pe care trebuie sa le aiba LED-urile, spunem
  //bibliotecii FastLED sa trimit comenzile corespunzatoare catre inelul de LED-uri
  FastLED.show();
  
  //asteptam 30 de milisecunde, ca sa controlam viteza cu care se modifica LED-urile
  //aditional, consumam mai putina energie cat timp asteptam.
  delay(30);
}
